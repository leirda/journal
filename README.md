<!--
SPDX-FileCopyrightText: 2020 Adriel Dumas--Jondeau <adrieldj@orange.fr>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# Simple journaling script made in bash

this is what I use to write, read & efficiently manage my journal.
